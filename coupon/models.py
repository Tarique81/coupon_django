# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from loyality_program.models import BaseClass
from django.contrib.auth.models import User
from app.models import Member
import datetime


# Create your models here.
class CouponValidity(BaseClass):
    days = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.days)


class Coupon(BaseClass):
    coupon_num = models.CharField(max_length=100, blank=True, null=True)
    invoice_num = models.CharField(max_length=100, blank=True, null=True)
    is_valid = models.BooleanField(default=False)
    is_survey = models.BooleanField(default=False)
    coupon_validity = models.ForeignKey(CouponValidity, on_delete=models.CASCADE, null=True, blank=True)
    member_id = models.ForeignKey(Member, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.coupon_num

    def check_validity(self):
        # coupon = Coupon.objects.get(coupon_num=coupon_num)
        coupon_life = (datetime.datetime.now() - self.created_date.replace(tzinfo=None)).days
        coupon_validity_days = CouponValidity.objects.get(days=20).days
        if coupon_life <= coupon_validity_days:
            days_remaining = coupon_validity_days - coupon_life
            message = str(days_remaining) + ' days remaining'
            return message
        else:
            days_remaining = coupon_life - coupon_validity_days
            message = 'invalid ' + str(days_remaining) + ' days ago'
            return message


class CouponIssuedBy(BaseClass):
    coupon_id = models.ForeignKey(Coupon, on_delete=models.CASCADE, null=True, blank=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
