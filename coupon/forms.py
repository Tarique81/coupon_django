from django import forms
from .models import Coupon
from app.models import Member


class IssueCouponForm(forms.ModelForm):
    coupon_num = forms.CharField(max_length=100)
    invoice_num = forms.CharField(max_length=100)

    class Meta:
        model = Coupon
        fields = ('coupon_num', 'invoice_num')


class CouponCheckForm(forms.ModelForm):
    email = forms.EmailField(max_length=100)

    class Meta:
        model = Coupon
        fields = ('coupon_num',)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not Member.objects.filter(email=email).exists():
            raise forms.ValidationError('Email does not exsist')
        return email
