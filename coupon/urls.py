from django.conf.urls import url
from django.views.generic import TemplateView
from app import views
from . import views as couponview

urlpatterns = [
    url(r'^home/$', couponview.coupon_home, name='coupon_home'),
    url(r'^issue/$', couponview.coupon_issue, name='issue'),
    url(r'^validity/$', couponview.coupon_validity, name='validity'),
]