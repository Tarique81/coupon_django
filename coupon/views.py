# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import getpass
import subprocess

from django.shortcuts import render, redirect
from .models import Coupon, CouponIssuedBy, CouponValidity
from .forms import IssueCouponForm, CouponCheckForm
from app.models import Member
from .utils import create_file
import string
import random
import datetime
import platform
import os


def id_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# Create your views here.
def coupon_home(request):
    return render(request, 'coupon/coupon_home.html')


def coupon_issue(request):
    if request.method == 'POST':
        form = IssueCouponForm(request.POST)
        if form.is_valid():
            coupon_num = form.cleaned_data.get('coupon_num')
            invoice_num = form.cleaned_data.get('invoice_num')
            coupon = Coupon(coupon_num=coupon_num, invoice_num=invoice_num)
            coupon.is_valid = True
            coupon.save()
            create_file(coupon.coupon_num)
            if platform.system() == 'Linux':
                lpr = subprocess.Popen("/usr/bin/lpr", stdin=subprocess.PIPE)
                lpr.stdin.write(coupon_num)
            if platform.system() == 'Windows':
                desktop = os.path.join(os.path.expanduser("~"), getpass.getuser(), "Desktop")
                os.startfile(os.path.join(desktop, 'coupon.txt'), "print")
            # coupon_issued_by=CouponIssuedBy(coupon_id=coupon.id, user_id=request.user)
            # coupon_issued_by.save()
            return redirect('home')
    else:
        coupon_num = str(id_generator())
        form = IssueCouponForm(request.GET, initial={'coupon_num': coupon_num})
    return render(request, 'coupon/coupon_home.html', {'form': form, 'coupon': coupon_num})


def coupon_validity(request):
    if request.method == 'POST':
        form = CouponCheckForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            coupon = Member.objects.get(email=email).coupon_set.all().filter(is_valid=True)
            if coupon is not None and len(coupon) == 1:
                coupon_check = coupon[0]
                coupon_life = (datetime.datetime.now() - coupon_check.created_date.replace(tzinfo=None)).days
                coupon_validity_days = CouponValidity.objects.get(days=20).days
                if coupon_life <= coupon_validity_days:
                    days_remaining = coupon_validity_days - coupon_life
                    message = str(days_remaining) + ' days remaining'
                else:
                    days_remaining = coupon_life - coupon_validity_days
                    message = 'invalid ' + str(days_remaining) + ' days ago'
                return render(request, 'coupon/coupon_validity.html', {'message': message})
            else:
                return render(request, 'coupon/coupon_validity.html',
                              {'message': 'This email id does not have any associated coupon..'})
    else:
        form = CouponCheckForm()
    return render(request, 'coupon/coupon_validity.html', {'form': form})
