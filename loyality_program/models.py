from django.db import models
import uuid


class BaseClass(models.Model):
    id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    class Meta:
        abstract = True


def convert_string_to_uuid(s):
    return uuid.UUID(s).hex