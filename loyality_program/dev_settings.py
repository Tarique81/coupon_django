from .settings import *
import dj_database_url
database_url = "postgres://postgres:test123@localhost:5432/bal"
DATABASES = {
    'default': dj_database_url.config(default=database_url)
}
