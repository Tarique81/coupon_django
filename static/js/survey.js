$(document).ready(function() {
    $(".selectpicker").on("change", function() {
        if ($(this).val() === "other") {
            $("#otherName").show();
        }
        else {
            $("#otherName").hide();
        }
    });
});