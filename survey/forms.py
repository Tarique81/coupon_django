from django import forms
from .models import Choice

class SurveyForm(forms.Form):
    choice= forms.CharField(max_length=100)
    question= forms.CharField(max_length=100)

class SurveyCreateForm(forms.Form):
    category = forms.CharField(max_length=100, required=False)
    new_category = forms.CharField(max_length=100, required=False)
    question = forms.CharField(max_length=100)
    choice1 = forms.CharField(max_length=100)
    choice2 = forms.CharField(max_length=100)
    choice3 = forms.CharField(max_length=100)
    choice4 = forms.CharField(max_length=100)
    def clean_category(self):
        category=self.cleaned_data.get('category')
        new_category= self.cleaned_data.get('new_category')
        if category==None and not new_category:
            raise forms.ValidationError('You must select a category or create another')
        return category,new_category
class CategoryForm(forms.Form):
    category = forms.CharField(max_length=100)