from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^home/', views.survey, name='survey'),
    url(r'^create/$', views.create_survey, name='create_survey'),
    url(r'^category/(?P<id>[\w-]+)/$', views.survey_detail, name='survey_detail'),
    url(r'^question/(?P<id>[\w-]+)/$', views.question_detail, name='question_detail'),
    url(r'^thanks/', TemplateView.as_view(template_name='home/thank_you.html'), name='thank_you'),
]