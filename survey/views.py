# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import urllib2

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.db.models import Q
from .models import Question, Answers, Choice, Category, Member_Category
from app.models import Member
from coupon.models import Coupon
# from django.forms import formset_factory
from .forms import SurveyCreateForm, CategoryForm
import json


# Create your views here.
def survey(request):
    if request.method == "POST":
        data = json.loads(json.dumps(dict(request.POST.iterlists())))
        for each in data['choice']:
            choice = Choice.objects.get(id=each.split(",")[0])
            question = Question.objects.get(id=each.split(",")[1])
            answer = Answers(question=question, choice=choice)
            answer.save()
        coupon = Coupon.objects.filter(Q(member_id__email=data['emial'][0]) or Q(is_valid=True))
        if len(coupon) == 1:
            for each in coupon:
                each.is_survey = True
                each.save()
        return redirect('thank_you')
    else:
        if request.GET.get('email') is not None:
            email = request.GET.get('email')
            if Member_Category.objects.all().filter(member__email=email):
                category = Category.objects.filter(
                    id__in=Member_Category.objects.all().filter(member__email=email).values_list('category_id'))
                if len(Category.objects.all().exclude(title__in=category.values_list('title')).order_by('?')) == 0:
                    return redirect('thank_you')

                survey_choice = Category.objects.all().exclude(title__in=category.values_list('title')).order_by('?')[
                                :1]
                question_set = Question.objects.all().filter(category__title__in=survey_choice.values_list('title'))
                # if category.title==title:
                member_category = Member_Category(member=Member.objects.get(email=email), category=survey_choice[0])
                member_category.save()
                return render(request, 'home/survey.html',
                              {'questions': question_set, 'survey': survey_choice, 'email': email})
            survey_choice = Category.objects.all().order_by('?')[:1]
            member_category = Member_Category(member=Member.objects.get(email=email), category=survey_choice[0])
            member_category.save()
            question_set = Question.objects.all().filter(category__title__in=survey_choice.values_list('title'))

    return render(request, 'home/survey.html', {'questions': question_set, 'survey': survey_choice, 'email': email})


def create_survey(request):
    if request.method == "POST":
        category = Category.objects.all()
        form = SurveyCreateForm(request.POST)
        if form.is_valid():
            new_category = form.cleaned_data.get('new_category')
            if new_category != '':
                category = Category(title=new_category)
                category.save()
            else:
                category = Category.objects.get(title=form.cleaned_data.get('category'))
            question = Question(category=category, question_text=form.cleaned_data.get('question'))
            question.save()
            choice1 = Choice(question=question, choice_text=form.cleaned_data.get('choice1'))
            choice2 = Choice(question=question, choice_text=form.cleaned_data.get('choice2'))
            choice3 = Choice(question=question, choice_text=form.cleaned_data.get('choice3'))
            choice4 = Choice(question=question, choice_text=form.cleaned_data.get('choice4'))
            choice1.save()
            choice2.save()
            choice3.save()
            choice4.save()
            return redirect(request.path_info)
    else:
        form = SurveyCreateForm()
        category = Category.objects.all()
    return render(request, 'survey/create_survey.html', {'category': category, 'form': form})


def survey_detail(request, id):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data.get('category')
            category = Category.objects.get(id=id)
            category.title = data
            category.save()
    form = CategoryForm()
    category = Category.objects.get(id=id)
    return render(request, 'survey/survey_detail.html', {'category': category, 'form': form})


def question_detail(request, id):
    if request.method == 'POST':
        data = json.loads(json.dumps(request.POST))
        question = Question.objects.get(id=id)
        for each in question.choice_set.all():
            if str(each.id) in data.keys():
                each.choice_text = data[str(each.id)]
                each.save()
        question.question_text = data[[each for each in data.keys() if 'q' in each][0]]
        question.save()
    question = Question.objects.get(id=id)
    return render(request, 'survey/question_detail.html', {'question': question})
