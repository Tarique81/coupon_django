# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Question, Answers, Choice, Category, Member_Category

# Register your models here.
class QuestionAdmin(admin.ModelAdmin):
    class Meta:
        model = Choice
        fields = ('question', 'choice_text')
    list_display = ('question', 'choice_text')
admin.site.register(Question)
admin.site.register(Answers)
admin.site.register(Choice, QuestionAdmin)
admin.site.register(Category)
admin.site.register(Member_Category)
