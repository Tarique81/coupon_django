from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.db.models import Count
from .models import Member
from coupon.models import Coupon
from .forms import RegisterCouponForm, RegisterInfoForm, MemberForm
import datetime

def home(request):
    return render(request, 'home/home.html')


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
        elif form.error_messages:
            error_message = form.error_messages
            return render(request, 'authentication/signup.html', {'form': form, 'error': error_message})


    else:
        form = UserCreationForm()
    return render(request, 'authentication/signup.html', {'form': form})


def all_members(request):
    members = Member.objects.all().annotate(coupon_count=Count('coupon'))
    return render(request, 'home/all_members.html', {'member': members})
def birthday_member(request):
    members = Member.objects.all()
    birthday_member_list =[]
    for each in members:
        if each.birthday!=None:
            if each.birthday>=datetime.datetime.now().date():
                if ((each.birthday-datetime.datetime.now().date()).days >= 0) and ((each.birthday-datetime.datetime.now().date()).days <= 7) and (each.birthday!=None):
                    birthday_member_list.append(each)

    return render(request,'home/birthday_member.html',{'bdmember':birthday_member_list})

def member_detail(request, id):
    if request.method == "POST":
        form = MemberForm(request.POST)
        member = Member.objects.get(id=id)
        if form.is_valid():
            full_name = form.cleaned_data.get('full_name')
            email = form.cleaned_data.get('email')
            phone = form.cleaned_data.get('phone')
            birthday = form.cleaned_data.get('birthday')
            member.full_name = full_name
            member.email = email
            member.phone = phone
            member.birthday=birthday
            member.save()
            return HttpResponseRedirect(request.path_info)
    else:
        member = Member.objects.get(id=id)
        form = MemberForm()
    return render(request, 'home/member_detail.html', {'form': form,'member':member})


def loyal_members(request):
    loyal_members = Member.objects.all().filter(is_loyal=True).annotate(coupon_count=Count('coupon'))
    return render(request, 'home/loyal_members.html', {'loyal_members': loyal_members})


def register_coupon(request):
    if request.method == 'POST':
        form = RegisterCouponForm(request.POST)
        if form.is_valid():
            coupon_num = form.cleaned_data.get('coupon_num')
            email = form.cleaned_data.get('email')
            if Coupon.objects.all().filter(coupon_num=coupon_num).exists() and Member.objects.all().filter(
                    email=email).exists():
                coupon = Coupon.objects.get(coupon_num=coupon_num)
                a = coupon.check_validity()
                if 'invalid' in a:
                    return render(request, 'home/register_coupon.html', {'message': a})
                else:
                    # return HttpResponseRedirect('home' + '?email=email')
                    return redirect('/survey/home' + '?email=' + email)
            elif Coupon.objects.all().filter(coupon_num=coupon_num).exists():
                email = Member(email=email)
                email.save()
                coupon = Coupon.objects.get(coupon_num=coupon_num)
                coupon.member_id = email
                coupon.save()
                return redirect('/info' + '?email=' + email.email)
            else:
                return render(request, 'home/register_coupon.html', {'message': 'Invalid Coupon Number'})


    else:
        form = RegisterCouponForm()
        message = ''
        if request.GET.get('message') is not None:
            message = request.GET.get('message')

    return render(request, 'home/register_coupon.html', {'form': form, 'message': ''})


def register_info(requset):
    if 'email' in requset.GET:
        query_string = requset.GET.get('email')
    if requset.method == 'POST':
        form = RegisterInfoForm(requset.POST)
        if form.is_valid():
            full_name = form.cleaned_data.get('full_name')
            phone = form.cleaned_data.get('phone')
            email = form.cleaned_data.get('email')
            member = Member.objects.get(email=email)
            member.full_name = full_name
            member.phone = phone
            member.save()
            return redirect('/survey/home' + '?email=' + email)
    else:
        form = RegisterInfoForm()
    return render(requset, 'home/register_info.html', {'form': form, 'query_string': query_string})
