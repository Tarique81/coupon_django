from django import forms
from coupon.models import Coupon

class RegisterCouponForm(forms.Form):
    coupon_num = forms.CharField(max_length=100, required=True)
    email = forms.EmailField(max_length=100, required=True)

    def clean_coupon_num(self):
        coupon_num= self.cleaned_data.get('coupon_num')
        if not Coupon.objects.filter(coupon_num=coupon_num).exists():
            raise forms.ValidationError('Coupon does not exsist')
        return coupon_num
class RegisterInfoForm(forms.Form):
    full_name = forms.CharField(max_length=100, required=True)
    phone = forms.CharField(max_length=100, required=True)
    email = forms.EmailField(max_length=100, required=True)

class MemberForm(forms.Form):
    full_name = forms.CharField(max_length=100, required=True)
    phone = forms.CharField(max_length=100, required=True)
    email = forms.EmailField(max_length=100, required=True)
    birthday = forms.DateField(required=False)

    def clean_full_name(self):
        full_name = self.cleaned_data.get('full_name')
        if not full_name:
            raise forms.ValidationError('Full Name is required')
        return full_name
    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not email:
            raise forms.ValidationError('email is required')
        return email
    def clean_phone(self):
        phone = self.cleaned_data.get('phone')
        if not phone:
            raise forms.ValidationError('phone is required')
        if not phone.isdigit():
            if '+' not in phone:
                raise forms.ValidationError('not a valid phone number')

        return phone