from django.conf.urls import url
from django.views.generic import TemplateView
from app import views
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.register_coupon, name='register_coupon'),
    url(r'^home/$', views.home, name='home'),
    url(r'^all_members/$', views.all_members, name='all_members'),
    url(r'^birthday_member/$', views.birthday_member, name='birthday_member'),
    url(r'^member/(?P<id>[\w-]+)/$', views.member_detail, name='member_detail'),
    url(r'^info/', views.register_info, name='info'),
    url(r'^loyal_members/$', views.loyal_members, name='loyal_members'),
    url(r'^login/$', auth_views.login,{'template_name': 'authentication/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'authentication/logout.html'}, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)