from django.db import models
from loyality_program.models import BaseClass
import datetime

class Member(BaseClass):
    email = models.EmailField(max_length=100, null=True, blank=True)
    phone = models.CharField(max_length=100, blank=True, null=True)
    birthday = models.DateField(null=True, blank=True)
    full_name = models.CharField(max_length=100, blank=True, null=True)
    is_loyal = models.BooleanField(default=False)

    def __str__(self):
        return self.email

    def birthday_near(self):
        if ((datetime.datetime.now()-self.birthday).days >= 0) and ((datetime.datetime.now()-self.birthday).days <= 7):
            return self